const request = require('request');
const shell = require('shelljs');
const _ = require('lodash');
let url = 'http://localhost:3000';
let newScript = '/user/scripts/new'; // http://localhost:3000/user/scripts/new
let submitScript = '/user/scripts/submit';
let iter = 0;

setInterval (() => {
  request(`${url}${newScript}`, (err, res, body) => {
    let data = JSON.parse(body);
    console.log(_.isEmpty(data));
    if (! _.isEmpty(data)) {
      let script = data.script;
      const {stdout, stderr, code} = shell.exec(`${script}`, {silent: true});
      data.response = stdout;
      console.log(data);
      request.post({
        uri: `${url}${submitScript}`,
        headers: {'content-type': 'application/json'},
        body: JSON.stringify(data)
      });
    }
  });
}, 1000);
