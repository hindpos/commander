let autonumber = 1;
let ioDivs = document.getElementsByClassName('container');
let ioTemplate = ioDivs[ioDivs.length - 1].innerHTML;
document.querySelector('.io-template').classList.add(`io${autonumber}`);
let onClickListenHelper = (socket) => {
  let input = document.querySelector(`div.io${autonumber} div input`).value;
  console.log(input);
  socket.emit('newScript', {
    script: input
  }, (_id) => {
    document.querySelector('.container').innerHTML += ioTemplate;
    autonumber ++;
    document.querySelector('.io-template:last-child').classList.add(`io${autonumber}`);
    document.querySelector(`.io${autonumber - 1} button`).disabled = true;
    document.querySelector(`.io${autonumber - 1}`).classList.add(`id_${_id}`);
  });
};
let submitResHelper = (res) => {
  console.log(`.id_${res._id} #txtarea`);
  document.querySelector(`.id_${res._id} #txtarea`).innerHTML = res.response;
}
