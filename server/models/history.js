const mongoose = require('mongoose');
var History = mongoose.model('History', {
  script: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  response: {
    type: String,
    minlength: 1,
    default: null
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: Number,
    default: null
  }
});

module.exports = {History};
