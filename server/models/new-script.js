const mongoose = require('mongoose');
var NewScript = mongoose.model('NewScript', {
  script: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: Number,
    default: null
  }
});

module.exports = {NewScript};
