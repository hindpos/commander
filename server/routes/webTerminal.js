const express = require('express');
const router = express.Router();

/* GET Web Terminal */
router.get('/', function(req, res, next) {
  res.render('webTerminal');
});

module.exports = router;
