const {ObjectID} = require('mongodb');

const {History} = require('./../../models/history');
const {NewScript} = require('./../../models/new-script');

const historyOneId = new ObjectID();
const historyTwoId = new ObjectID();
const newScriptOneId = new ObjectID();
const newScriptTwoId = new ObjectID();

const historyOneScript = "history one script";
const historyTwoScript = "history two script";
const newScriptOneScript = "history one script";
const newScriptTwoScript = "history two script";

const newScriptOneResponse = "newscript one response";
const newScriptTwoResponse = "newscript two response";

const history = [{
  _id: historyOneId,
  script: historyOneScript
}, {
  _id: historyTwoId,
  script: historyTwoScript
}];

const newScript = [{
  _id: newScriptOneId,
  script: newScriptOneScript,
  response: newScriptOneResponse
}, {
  _id: newScriptTwoId,
  script: newScriptTwoScript,
  response: newScriptTwoResponse
}];

const populateHistory = (done) => {
  History.remove({}).then(() => {
    return History.insertMany(history);
  }).then(() => done());
};

const populateNewScript = (done) => {
  newScript.remove({}).then(() => {
    return NewScript.insertMany(newScript);
  }).then(() => done());
};
module.exports = {history, newScript, populateHistory, populateNewScript};
