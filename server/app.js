const config = require('./config/config');

const express = require('express');
const expressHandlebars = require ('express-handlebars');
const _ = require('lodash');
const bodyParser = require('body-parser');
const path = require ('path');
const http = require('http');
const socketIO = require('socket.io');

const {mongoose} = require('./db/mongoose');
const {History} = require('./models/history');
const {NewScript} = require('./models/new-script');

var app = express();
var port = process.env.PORT;

var server = http.createServer(app);
var io = socketIO(server);

// view engine setup
const handlebars = expressHandlebars.create ({defaultLayout: 'default'});
app.engine ('handlebars', handlebars.engine);
app.set ('view engine', 'handlebars');
app.use (express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//adding routes
const webTerminal = require ('./routes/webTerminal');
// setting routes
app.use ('/webTerminal', webTerminal);

//routes
app.post('/admin/scripts/new', (req, res) => {
  var body = _.pick(req.body, ['script']);
  var newScript = new NewScript({
    script: body.script
  });

  newScript.save().then((script) => {
    res.send(script);
  }).catch((err) => res.status(400).send(err));
});

app.get('/admin/scripts/history', (req, res) => {
  History.find().then((scripts) => {
    res.send({scripts});
  }).catch((err) => res.status(400).send(err));
});

app.get('/user/scripts/new', (req, res) => {
  NewScript.findOne().then((script) => {
    script = _.pick(script, ['_id', 'script']);
    res.send(script);
  }).catch((err) => res.status(400).send(err));
});

let submitRes = 0;
// Socket.io functions
io.on('connection', (socket) => {
  socket.on('newScript', (script, callback) => {
    var newScript = new NewScript(script);
    newScript.save().then(() => {
      console.log(script);
      callback(newScript._id);
    }).catch(err => console.log(err));
  });
  submitRes = (script) => socket.emit('submitResponse', script);
});

app.post('/user/scripts/submit', (req, res) => {
  var body = _.pick(req.body, ['_id', 'script', 'response']);
  console.log(body);
  NewScript.findByIdAndRemove(body._id).then((script) => {
    script.response = body.response;
    var script2 = _.pick(script, ['_id', 'script', 'response']);
    return script2;
  }).then((script) => {
    var doneScript = new History(script);
    return doneScript.save();
  }).then((script) => {
    submitRes(script);
  }).catch((err) => {console.log(err); res.status(400).send(err);});
});

server.listen(port, () => {
  console.log(`Started server on port ${port}`);
});

module.exports = {app};
